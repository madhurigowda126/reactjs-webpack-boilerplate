import { Heading } from './Heading'

export default {
  title: 'Atoms/Heading',
  component: Heading,
  argTypes: {
    level: 'h1'
  }
}

const Template = (args) => <Heading {...args}>Page Heading</Heading>

export const Primary = Template.bind({})
Primary.args = {
  primary: true,
  label: 'Heading'
}
